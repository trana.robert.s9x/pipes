#include <iostream>
#include <vector>
#include <unistd.h>
bool is_prime(int num) {
    if (num < 2) {
        return false;
    }
    for (int i = 2; i * i <= num; ++i) {
        if (num % i == 0) {
            return false;
        }
    }
    return true;
}
void find_primes_in_range(int start, int end, int write_fd) {
    for (int i = start; i <= end; ++i) {
        if (is_prime(i)) {
            write(write_fd, &i, sizeof(i));
        }
    }
    close(write_fd);
}
int main() {
    const int num_processes = 10;
    const int range_size = 1000;
    const int max_number = 10000;
    int pipes[num_processes][2];
    for (int i = 0; i < num_processes; ++i) {
        if (pipe(pipes[i]) == -1) {
            perror("Pipe creation failed");
            return 1;
        }
        pid_t pid = fork();

        if (pid == -1) {
            perror("Fork failed");
            return 1;
        }
        if (pid == 0) {
            // Child process
            close(pipes[i][0]);  
            find_primes_in_range(i * range_size + 1, (i + 1) * range_size, pipes[i][1]);
            return 0;
        } else {
            // Parent process
            close(pipes[i][1]);  
        }
    }
    std::cout << "Prime numbers: ";
    for (int i = 0; i < num_processes; ++i) {
        int prime;
        while (read(pipes[i][0], &prime, sizeof(prime)) > 0) {
            std::cout << prime << " ";
        }
        close(pipes[i][0]);  
    }
    std::cout << std::endl;
    return 0;
}
